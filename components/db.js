require('dotenv').config()
import logger from '../components/logger'
import mysql from 'mysql'

export default {

  connected: false,
  connHandler: null,

  'connect': function () {
    let _DBSERVER = process.env.DBSERVER
    let _DBNAME = process.env.DBNAME
    let _DBUSER = process.env.DBUSER
    let _DBPWD = process.env.DBPWD
        
    this.connHandler = mysql.createConnection({
      host: _DBSERVER,
      user: _DBUSER,
      password: _DBPWD,
      database: _DBNAME
    })

    this.connHandler.connect((err) => {
      if (err) {
        logger.error('[db] Error connecting to MySQL: [' + err.stack + ']')
        return
      }
      this.connected = true
      logger.info('[db] Succsfully connected to MySQL as [' + this.connHandler.threadId + ']')
    })
  },
  
  'addSensorData': function (site, sensor, value) {
    let _payload = {
      'site': site,
      'sensor': sensor,
      'value': value
    }
    try {
      let _query = this.connHandler.query('INSERT INTO sensordata SET ?', _payload, (error, results, fields) => {
        if (error) throw error;
        logger.debug('[db/addSensorData] Added [' + site + '/' + sensor + '/' + value + '] to DB')
      })
    } catch (e) {
      logger.error('[db/addSensorData] Error adding [' + site + '/' + sensor + '/' + value + '] to DB: [' + e + ']')
    }
  },

  getSensorData: function () {
    let query = 'SELECT ts, site, sensor, value FROM sensordata'
    
    this.connHandler.query(query, function (err, rows) {
      if (err) throw err
      logger.info('[db/getSensorData] Recuperati [' + rows.length + '] record')
      return rows
    })
  }
}
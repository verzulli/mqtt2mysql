# builder - è il primo step, dove installo tutto l'ambaradan completo
# vvvvvvvv builder vvvvvvvvvvv
FROM node:12-alpine as builder

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .

# If you are building your code for production
#RUN npm ci --only=production
RUN npm install
RUN npm install -g @vercel/ncc
RUN npm run build
# ^^^^^^ fine builder ^^^^^^^

# Inizio seconda fase, dove copio solo il compilato
# vvvvvvvvvvvvvvvvvvvvvvvvvvvv
FROM node:12-alpine

# Create app directory
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/dist .
CMD [ "node", "index.js" ]
# ^^^^^^ fine secondo step ^^^^^

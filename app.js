require('dotenv').config({path: __dirname + '/.env'})
import logger from './components/logger'
import mqtt from 'mqtt'
import db from './components/db'

logger.info('[MAIN] Starting app')

try {
  logger.debug('Connecting to MQTT broker [' + process.env.MQTT_BROKER_URI + ']')
  const client = mqtt.connect(
    process.env.MQTT_BROKER_URI, 
    {
      "username": process.env.MQTT_USER,
      "password": process.env.MQTT_PASS
    }
  )

  logger.debug('Connecting to MySQL backend...')
  db.connect()

  logger.debug('All done! Just waiting for events...')

  //
  // 'connect' handler
  // 
  client.on('connect', function () {
    logger.debug('[onConnect] connected to broker !')
    logger.debug('[onConnect] subscribing to topic!')
    client.subscribe(process.env.MQTT_GLOBAL_PREFIX + '/#', (err) => {
      if (!err) {
        logger.debug('[onConnect] succesfully subscribed!')
      } else {
        logger.debug('[onConnect] unable to subscribe!')
      }
    })
  })

  // 
  // 'message' handler
  //
  client.on('message', function (topic, message) {
    logger.debug('[onMessage] Got a message!')
    // message is Buffer so neet to be casted
    logger.debug('[onMessage] topic: [' + topic + '] - Message: [' + message.toString() + ']')
    db.addSensorData(process.env.MQTT_GLOBAL_PREFIX,topic,message)
  })

} catch(e) {
  logger.error('[Errore] -->' + JSON.stringify(e) + '<--')
  process.exit()
}
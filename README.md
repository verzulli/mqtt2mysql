# MQTT to MySQL gateway

This is a simple NodeJS app, relaying messages received from an MQTT topic (sent by an MQTT broker) toward a MySQL database.

In other words, it feed a MySQL backend with data received via MQTT.

I'm developing it in order to store telemetry data from several IoT devices that I'm planning to deploy aroung my house... :-)

## 1 - Some more details
When launched:
* connect to an MQTT broker (`MQTT_BROKER_URI`) authenticating with username/password (`MQTT_USER`, `MQTT_PASS`);
* subscribe to a `topic` (actually, a wildcard-topic made by a `MQTT_GLOBAL_PREFIX` with a `/#` added);
* connect to a MySQL Server (`DB_*`) where a `sensordata` table is expected to be existing. The structure of the table is the following:

```
CREATE TABLE sensordata (
  ts timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  site varchar(100) DEFAULT NULL,
  sensor varchar(100) DEFAULT NULL,
  value decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (ts)
)
```

...and then, simply wait for MQTT messages and relay them to sensordata table


## 2 - What works so far

* succesfully receive MQTT messages;
* succesfully write them to MySQL;

## 3 - What need to be done

* parsing incoming topic to retrieve sensor-family and sensor-name. Remember that it's a "wildcard" topic, so it's similar to:
`dvhome/temp/s01` or `dvhome/hum/sensor2`. Hence we need to retrieve `temp` and `hum` (for `sensor type`) and `s01` / `sensor2` (for `sensor name`)

* better handling MQTT events (disconnect, error, etc.)

* better handle MySQL events (disconnect, reconnect, error)

## 4 - How can I test and run it?

after ensuring that NodeJS is installed on your system, just prepare the whole system:

* clone the project
* `cd` to the main project folder
* launch an `npm install`

and then, launch the application with:
* `npm start`

## 5 - I need help!
If you need help, you can try reaching me on Telegram, at `@Damiano_Verzulli`